#############################################################################
## GENERIC MAKEFILE FOR LATEX BY HEEDONG GOH <wellposed@gmail.com> ##########
## https://gitlab.com/hgoh/makefile-for-latax ###############################
#############################################################################
# Source directory
SDIR = .
# Output directory
ODIR = .
#############################################################################
## DO NOT CHANGE ############################################################
#############################################################################
LATEX  = lualatex
BIBTEX = bibtex
SRC   := $(shell cd $(SDIR) && grep -El '^[^%]*\\begin\{document\}' *.tex)
TRG    = $(SRC:.tex=.pdf)
TRGF   = $(SRC:.tex=_flat.pdf)
TRGD   = $(SRC:.tex=_flatd.pdf)
TRGP   = $(SRC:.tex=_print.pdf)
AUX    = $(SRC:.tex=.aux)
MIS    = {$(SRC:.tex=.aux),$(SRC:.tex=.log),$(SRC:.tex=.bbl),$(SRC:.tex=.blg)}
MIS2   = {$(SRC:.tex=.out),$(SRC:.tex=.toc),$(SRC:.tex=.fff),$(SRC:.tex=.lof)}
MIS3   = {$(SRC:.tex=.bcf),$(SRC:.tex=.nav),$(SRC:.tex=.snm),$(SRC:.tex=.run.xml)}
MIS4   = {$(SRC:.tex=Notes.bib),$(SRC:.tex=.spl)}
PODIR  = $(realpath .)/$(ODIR)
PSDIR  = $(realpath .)/$(SDIR)

all: run
run:
	mkdir -p $(ODIR)	
	cd $(SDIR) && $(LATEX) -output-directory=$(PODIR) $(SRC)
	sed -i 's@\\bibdata{@\\bibdata{$(PSDIR)\/@g' $(ODIR)/$(AUX)
	cd $(ODIR) && $(BIBTEX) $(AUX) ||true
	cd $(SDIR) && $(LATEX) -output-directory=$(PODIR) $(SRC)
	@echo -e '\n***** FINAL RUN *****\n'
	cd $(SDIR) && $(LATEX) -output-directory=$(PODIR) $(SRC)
one:
	cd $(SDIR) && $(LATEX) -output-directory=$(PODIR) $(SRC)
flat:
	cd $(ODIR) && gs -dSAFER -dBATCH -dNOPAUSE -dNOCACHE \
	-sDEVICE=pdfwrite \
	-sColorConversionStrategy=LeaveColorUnchanged \
	-dAutoFilterColorImages=true \
	-dAutoFilterGrayImages=true \
	-dDownsampleMonoImages=false \
	-dDownsampleGrayImages=false \
	-dDownsampleColorImages=false \
	-sOutputFile=$(TRGF) $(TRG)
flatd:
	cd $(ODIR) && gs -dSAFER -dBATCH -dNOPAUSE -dNOCACHE \
	-sDEVICE=pdfwrite \
	-sColorConversionStrategy=LeaveColorUnchanged \
	-dAutoFilterColorImages=true \
	-dAutoFilterGrayImages=true \
	-dDownsampleMonoImages=true \
	-dDownsampleGrayImages=true \
	-dDownsampleColorImages=true \
	-sOutputFile=$(TRGD) $(TRG)
print:
	cd $(ODIR) && gs -dSAFER -dBATCH -dNOPAUSE -dNOCACHE \
	-sDEVICE=pdfwrite \
	-sColorConversionStrategy=LeaveColorUnchanged \
	-dAutoFilterColorImages=true \
	-dAutoFilterGrayImages=true \
	-dPDFSETTINGS=/printer \
	-sOutputFile=$(TRGP) $(TRG)
clean:
	@rm -f $(ODIR)/$(MIS)
	@rm -f $(ODIR)/$(MIS2)
	@rm -f $(ODIR)/$(MIS3)
	@rm -f $(ODIR)/$(MIS4)
clobber: clean
	@rm -f $(ODIR)/$(TRG)
	@rm -f $(ODIR)/$(TRGF)
	@rm -f $(ODIR)/$(TRGD)
	@rm -f $(ODIR)/$(TRGP)
	@rm -i *.aux *.log *.bbl *.blg *.toc *.fff *.out *.lof *.bcf *.nav *.snm *.run.xml *.Notes.bib *.spl
help:
	@echo -e 'Usage: make [OPTION]'
	@echo -e 'Generic makefile for LaTeX\n'
	@echo -e 'Options:'
	@echo -e 'one\t\truns a single $(LATEX)'
	@echo -e 'flat\t\tproduces flattend pdf using ghostscript'
	@echo -e 'flatd\t\tproduces downsampled flattend pdf using ghostscript'
	@echo -e 'clean\t\tremoves auxiliary files'
	@echo -e 'clobber\t\tremoves auxiliary files and the pdf file'
	@echo -e 'help\t\tprints this message'
.phony: all, run, flat, flatd, print, clean, clobber, help
#############################################################################